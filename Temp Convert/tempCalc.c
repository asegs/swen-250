#include <stdlib.h>
#include <stdio.h>
void main(){
	printf("Fahrenheit-Celsius\n");
	float c=0;
	for (int f=0;f<200;f+=20){
		c=((f-32.0)*(5.0/9.0));
		printf("%dF = %fC\n",f,c);
	}
}
