/*
 * Skeleton implementation for the activity to parse and print
 * CSV formatted files read from standard input.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "csv.h"
#include "unit_tests.h"
#define MAX_FIELDS (15)
#define MAX_CHARS (20)
#define MAX_STRING (MAX_FIELDS*MAX_CHARS)
typedef char f_string[MAX_CHARS + 1] ;

typedef struct {

    int nfields ;

    f_string field[MAX_FIELDS] ;

} csv_line ;



/*
 * Returns true iff the character 'ch' ends a field. That is, ch is end of file,
 * a comma, or a newline.
 */

bool is_end_of_field(int ch) {
	return (ch == ',') || (ch == '\n') || (ch == EOF) ;
}

/*
 * Return the minimum of two integers.
 */

int min(int x, int y) {
	return x < y ? x : y ;
}

/*
 * Read the next field from standard input. Returns the value of getchar() that
 * stopped (terminated) the field.
 */

int get_field(f_string field) {
	char ch=getchar();
	if (is_end_of_field(ch)){
		return ch;
	}
}

/*
 * Read in a CSV line. No error checking is done on the number of fields or
 * the size of any one field.
 * On return, the fields have been filled in (and properly NUL-terminated), and
 * nfields is the count of the number of valid fields.
 * nfields == 0 means end of file was encountered.
 */

csv_line get_line() {
	char ch;
	csv_line line;
	int place=0;
	while (true){
		ch=getchar();
		if (ch==','){
			line.nfields+=1;
			place=0;		
		}
		if (!is_end_of_field(ch)){
			line.field[line.nfields][place]=ch;
			place+=1;
		}
		if (ch=='\n'||ch==EOF){
			break;
		}
	}
	return line;
	/* FILL THIS IN */
}

/*
 * Print a CSV line, associating the header fields with the
 * data line fields.
 * The minimum of the number of fields in the header and the data
 * determines how many fields are printed.
 */

void print_csv(csv_line header, csv_line data) {
	for (int i=0;i<header.nfields;i++){
		printf("%s=%s\n",header.field[i],data.field[i]);
	}
}

/*
 * Driver - read a CSV line for the header then read and print data lines
 * until end of file.
 */

int main( int argc, char *argv[] ) {
	csv_line header ;
	csv_line current ;

	// LLK additional if statement to execute unit tests if program
	// name contains "test".
	FILE *fp;
	fp = fopen(argv[1], "r");
    	if (fp == NULL){
        	printf("%s is not a real file.\n",fp);
        	return 1;
    	}
	char string[MAX_STRING];
	while(fgets(string,MAX_STRING,fp)!=NULL){
		
		}
		
	if ( strstr( argv[0], "test" ) )
		return test() ;
	header = get_line() ;
	current = get_line() ;

	while ( current.nfields > 0 ) {
		print_csv(header, current) ;
		current = get_line() ;
	}

	return 0 ;
}
