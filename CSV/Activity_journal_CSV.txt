SWEN-250 Activity Reflection

Name: Aaron Segal

================================================================
Implement code to pass all unit tests

Estimated Time	2H 0M
Plan:
Implement basic functions, use the two dimensional arrays to handle putting data in, get the file from the argv.

Complete
Actual Time	3H 30M
Observations:
Constant errors with conflicting types did not let program run or test.  Possibly due to the operating system?  This was causing compatability issues before.  The get_line function was complex because we needed to implement it with structs.  The field part of csv_line was two dimensional, and so I filled the fields.  For each comma the line contained, I increased the number of fields by one and reset the place in the subfield to zero.  When a normal character was found, I placed it in the subfield at the current value of place and set the position to place the next field in to the next one.  When an end of line character was detected, I returned the csv_line.



================================================================
Test correct operation from the command line

Estimated Time	1H 0M
Plan:
Use printf to debug errors, get the argvs as filename, and runs proper tests.
Complete
Actual Time	0H 15M
Observations:
File would not compile, reduced error count significantly but could not run due to conflicting types errors for all uses of csv_line.


================================================================
